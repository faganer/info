=== info ===

Contributors: 更好的WordPress主题
Tags: responsive, carousel, self-media, Multi-user, multisite, blog

Requires at least: 4.5
Tested up to: 5.4
Requires PHP: 5.6
Stable tag: 1.5
License: GNU General Public License v2 or later
License URI: LICENSE

A starter theme called info.

== Description ==

适用于个人博客、自媒体、新闻资讯、多用户的WordPress主题。

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

= 1.5 - 2021-08-20 =
* 修正archive、search页面分类不显示问题


= 1.4 - 2021-06-15 =
* 修正iOS搜索框无法输入内容的问题
